#include "glslrenderwidget.h"

#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLVersionProfile>
#include <QOpenGLContext>
#include <QSurfaceFormat>
#include <QTime>
#include <QTimerEvent>

GLSLRenderWidget::GLSLRenderWidget() :
    m_timerId(-1) // initialize with -1
{
    setMinimumSize(600,600);

    m_time = new QTime;
    m_time->start();
}

GLSLRenderWidget::~GLSLRenderWidget(){
    delete m_vao;
    delete m_vbo;
    delete m_ibo;
    delete m_program;
    delete m_time;
}

/*
 * This function is automatically called after constructor
 */
void GLSLRenderWidget::initializeGL(){

    initializeOpenGLFunctions();

    glClearColor(1.0f,1.0f,1.0f,1.0f);

    m_vao = new QOpenGLVertexArrayObject;
    m_vbo = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    m_ibo = new QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);

    m_vao->create();
    m_vao->bind();

    constexpr float vertices[8] = {
         1.0f, 1.0f,
        -1.0f, 1.0f,
        -1.0f,-1.0f,
         1.0f,-1.0f
    };

    m_vbo->create();
    m_vbo->bind();
    m_vbo->allocate(vertices,sizeof(float)*8);

    glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,sizeof(float)*2,nullptr);
    glEnableVertexAttribArray(0);

    constexpr GLuint indices[6] = {
        0,1,2,
        2,3,0
    };

    m_ibo->create();
    m_ibo->bind();
    m_ibo->allocate(indices,sizeof(GLuint)*6);

    m_vao->release();

    m_version = context()->format().version();
    const int major = m_version.first;
    const int minor = m_version.second;
    m_versionStr = QString("#version %0%1").arg(major).arg(minor) + QString("0\n");

    m_vShaderCode = m_versionStr
            + "layout(location = 0) in vec4 VertexPosition;\n"
              "void main(){\n"
              "    gl_Position = VertexPosition;\n"
              "}\n";
    m_fShaderCode = m_versionStr
            + "layout(location = 0) out vec4 FragColor;\n"
              "uniform vec2 resolution;\n"
              "uniform float time;\n\n"
              "void main(){\n"
              "    FragColor = vec4(1.0,0.0,0.0,1.0);\n"
              "}\n";

    m_program = new QOpenGLShaderProgram;
    m_program->addShaderFromSourceCode(QOpenGLShader::Vertex,m_vShaderCode);
    m_program->addShaderFromSourceCode(QOpenGLShader::Fragment,m_fShaderCode);
    m_program->link();

    emit changeGLSLCode(m_fShaderCode);
}

void GLSLRenderWidget::resizeGL(int w, int h){
    m_resolution = QVector2D(w,h);
    m_program->setUniformValue("resolution",m_resolution);
}

void GLSLRenderWidget::paintGL(){

    makeCurrent();

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);


    m_program->bind();
    m_program->setUniformValue("time",m_time->elapsed()/1000.0f);
    m_vao->bind();

    glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_INT,nullptr);

    m_vao->release();
    m_program->release();
}

void GLSLRenderWidget::setGLSLCode(const QString& f_shader){
    if(f_shader == m_fShaderCode) return;
    QOpenGLShaderProgram *tmp = new QOpenGLShaderProgram;
    tmp->addShaderFromSourceCode(QOpenGLShader::Vertex,m_vShaderCode);
    tmp->addShaderFromSourceCode(QOpenGLShader::Fragment,f_shader);
    if(!tmp->link()){
        delete tmp;
        return;
    }
    delete m_program;
    m_fShaderCode = f_shader;
    m_program = tmp;
    update();
}

void GLSLRenderWidget::timerEvent(QTimerEvent *event){
    if(event->timerId() == m_timerId){
        update();
    }
}

void GLSLRenderWidget::restartTimer(){
    m_time->restart();
    setGLSLCode(m_fShaderCode);
}

void GLSLRenderWidget::setRenderMode(bool mode){
    if(mode){
        m_timerId = startTimer(0);
    }else{
        killTimer(m_timerId);
        m_timerId = -1;
    }
}
