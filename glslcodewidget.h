#ifndef GLSLCODEWIDGET_H
#define GLSLCODEWIDGET_H

#include <QWidget>
#include <QPlainTextEdit>

class LineNumberArea;

class GLSLCodeWidget : public QPlainTextEdit
{
    Q_OBJECT
    Q_DISABLE_COPY(GLSLCodeWidget)
public:
    GLSLCodeWidget(QWidget *parent = Q_NULLPTR);
\
    int lineAreaWidth();
    void lineAreaPaintEvent(QPaintEvent *event);

public slots:
    void updateLineAreaWidth(int);
    void updateLineArea(const QRect &rect, int dy);

protected:
    void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;

private:
    LineNumberArea *m_lineNumber; // The implementation is below
};




class LineNumberArea : public QWidget{

public:
    explicit LineNumberArea(GLSLCodeWidget *code) :
        QWidget(code),
        m_code(code)
    {}

    QSize sizeHint() const Q_DECL_OVERRIDE{
        return QSize(m_code->lineAreaWidth(),0);
    }

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE{
        m_code->lineAreaPaintEvent(event);
    }
private:
    GLSLCodeWidget *m_code;
};

#endif // GLSLCODEWIDGET_H
