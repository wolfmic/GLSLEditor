#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class GLSLControlWidget;
class GLSLRenderWidget;
class QHBoxLayout;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = Q_NULLPTR);
    ~MainWindow();

private:
    QWidget           *m_cw;
    QHBoxLayout       *m_layout;
    GLSLRenderWidget  *m_renderWidget;
    GLSLControlWidget *m_controlPanel;
};

#endif // MAINWINDOW_H
