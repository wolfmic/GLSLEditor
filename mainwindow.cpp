#include "mainwindow.h"

#include "glslcontrolwidget.h"
#include "glslrenderwidget.h"

#include <QHBoxLayout>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    m_cw           = new QWidget;
    m_layout       = new QHBoxLayout;
    m_renderWidget = new GLSLRenderWidget;
    m_controlPanel = new GLSLControlWidget;

    m_layout->addWidget(m_controlPanel,Qt::AlignLeft);
    m_layout->addWidget(m_renderWidget,Qt::AlignRight);
    m_cw->setLayout(m_layout);
    setCentralWidget(m_cw);

    connect(m_controlPanel,&GLSLControlWidget::changeGLSLCode,
            m_renderWidget,&GLSLRenderWidget::setGLSLCode);
    connect(m_renderWidget,&GLSLRenderWidget::changeGLSLCode,
            m_controlPanel,&GLSLControlWidget::setGLSLCode);
    connect(m_controlPanel,&GLSLControlWidget::setRenderMode,
            m_renderWidget,&GLSLRenderWidget::setRenderMode);
    connect(m_controlPanel,&GLSLControlWidget::restart,
            m_renderWidget,&GLSLRenderWidget::restartTimer);

}

MainWindow::~MainWindow()
{

}
